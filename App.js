import React, { useState } from 'react';
import { StyleSheet, Text, View, Switch, TextInput, Button, Image } from 'react-native';

export default function App() {
  const [bitcoinAcceptSwitch, setBitcoinAcceptSwitch] = useState(false);
  const [cashAcceptSwitch, setCashAcceptSwitch] = useState(false);

  return (
    <View style={styles.container}>
      <View style={styles.heading}>
        <Image 
          style={styles.greetingImage}
          source={require('./assets/bitquid.png')}
        />
        <Text>Personally Exchange Bitcoin and Cash!</Text>
        <Text style={{padding:10}}>Indicate if you can act as an ATM for a fellow crypto user.  When you toggle your switch you will show up on the map to help a buddy!</Text>
      </View>
      <View>
        <View style={styles.settingsToggles}>
          <Switch
            onValueChange={() => setBitcoinAcceptSwitch(!bitcoinAcceptSwitch) }
            value={bitcoinAcceptSwitch}
          />
          <Text> Accept Bitcoin for Cash</Text>
        </View>
        <View style={styles.settingsToggles}>
          <Switch
            onValueChange={() => setCashAcceptSwitch(!cashAcceptSwitch) }
            value={cashAcceptSwitch}
          />
          <Text> Accept Cash for Bitcoin</Text>
        </View>
      </View>
      <View style={{flexDirection:'row'}}>
        <Button 
          onPress={() => setCashAcceptSwitch(!cashAcceptSwitch)}
          title="Go To Map"
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ccc000',
    alignItems: 'center',
    justifyContent: 'center',
  },
  heading: {
    alignItems: 'center',
  },
  greetingImage: {
    width: 230,
    height: 66,
    alignSelf: 'center',
    backgroundColor: '#ccc000'
  },
  settingsToggles: {
    flexDirection: 'row',
    padding: 10,
    alignItems: 'center'
  },
  addGoalInput: {
    borderBottomWidth: 1,
    borderBottomColor: '#000000',
    width: 230,
    padding: 3
  }
});

